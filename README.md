# Asana-RTL-CRX
![Static Badge](https://img.shields.io/badge/Chrome_Webstore_Status:-Published-green)
![GitLab License](https://img.shields.io/gitlab/license/SM26%2Fasana-rtl-crx)

![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/SM26%2Fasana-rtl-crx)

## Name
Asana RTL

## Description
Adds RTL support in Asana's website. only work's for Asana domain.<br>
Does **not** work for the Asana desktop app - consider using the PWA version, see explanation below.

## Chrome webstore stats
(this section should get populated automatically when the extension will get approved in the store.)<br>
![Chrome Web Store Version](https://img.shields.io/chrome-web-store/v/kndckhbjkdnckpngdofaggnbgfnpgblb)
![Chrome Web Store Users](https://img.shields.io/chrome-web-store/users/kndckhbjkdnckpngdofaggnbgfnpgblb)<br>
![Chrome Web Store](https://img.shields.io/chrome-web-store/stars/kndckhbjkdnckpngdofaggnbgfnpgblb)
![Chrome Web Store](https://img.shields.io/chrome-web-store/rating-count/kndckhbjkdnckpngdofaggnbgfnpgblb)
![Chrome Web Store](https://img.shields.io/chrome-web-store/rating/kndckhbjkdnckpngdofaggnbgfnpgblb)
<br>appid: [kndckhbjkdnckpngdofaggnbgfnpgblb](https://chromewebstore.google.com/detail/kndckhbjkdnckpngdofaggnbgfnpgblb)


<h2>Installation
<h3>Automatic (recommended)</h3>
just download it from the chrome store, [link](https://chromewebstore.google.com/detail/kndckhbjkdnckpngdofaggnbgfnpgblb).
<h3>Manual method</h3>
1. download this repo, and unzip it into a folder. <br>
2. go to [chrome://extensions](chrome://extensions)<br>
3. make sure "Developer mode" is on<br>
4. use "load unpacked" and point it to the folder you downloaded<br>

<details>
<summary><h2>PWA - Alternative to the app</h2></summary>
Windows support PWA, which means that you can add a desktop shortcut and the site will behave like an app(!)
To do so, here is an example of how to do it in Chrome
(It works on other browsers, with minor differences)

1. While on the main page (this one), click on the 3 vertical dots at the top right corner.
2. Click on "more tools"
3. Then "create shortcut..."
![!PWA1](https://sm26.gitlab.io/swm_wiki/assets/images/PWA1.webp){.medium}
1. In the pop up box, be sure to check the "open as window" box
![!PWA2](https://sm26.gitlab.io/swm_wiki/assets/images/PWA2.webp){.medium}
1. Profit!

Now you should see a shortcut on your desktop, and you can view it as a separate app and not as a Chrome tab.
</details>



## Support
![haha, no.](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3VFB64gPsjDIta0hHi8nx8IDd6gibn2cP8w&usqp=CAU)<br>
open an issue and hope for the best.

<details><summary><h2>Contributing - WIP</summary>
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.
</details>

## Authors and acknowledgment
I've learned a lot from [nimayazdi93/asana-rtl-chrome-extension](https://github.com/nimayazdi93/asana-rtl-chrome-extension), and this extension is **heavily** based on Nimayazdi93's work.

<details>
<summary><h2>Project status</summary>
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
</details>
