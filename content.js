// Create a <style> element
const styleElement = document.createElement('style');

// Define the CSS styles
// setting !Important to override the margin-left, is a BAD IMPLEMENTATION! but I'm stupid
const cssStyles = `
  p {
   direction:rtl
  }
  span{
    direction:rtl
  }
  input{
    direction:rtl
  }
  itemrow{
    direction:rtl
  }
  textarea{
    direction:rtl
  }
  .FeedBlockStory{
    direction:rtl
  }
  .TaskList{
    direction:rtl
  }
  .TypographyPresentation{
    direction:rtl
  }
  .DragHandle{
    margin-left: -10px
  }
  .DraggableTaskRow-dragHandle{
    margin-left: auto
  }
  .DragHandle.DraggableTaskRow-dragHandle{
    margin-left: auto
  }
  .DraggableTaskRow--withMiniIcon .DraggableTaskRow-dragHandle{
    margin-left: -10px !important
  }
`;

// Set the CSS styles as the content of the <style> element
styleElement.innerHTML = cssStyles;

// Append the <style> element to the document's <head>
document.head.appendChild(styleElement);